#!/usr/bin/python

from collections import Counter
import yaml

def calculate_damage(base_damage, skill_multi, skill_flat, damage_bonus, enemy_resistance):

    print(base_damage, skill_multi, skill_flat, damage_bonus, enemy_resistance)
    return (((base_damage * skill_multi) + skill_flat) *
            (1+damage_bonus) * (1-enemy_resistance/100))

def load_weapons(config):
    return config['weapons']

def load_enemies(config):
    return config['enemies']

def load_skills(config):
    return config['skills']

def load_calculations(config):
    return config['calculations']

def load_bonuses(config):
    return config['bonuses']

def calculate_gear_bonuses(gear):
    ret = Counter({})
    for item in gear:
        ret = Counter(gear[item]) + ret
    return ret


config = {}
with open('builds/speedy.yml', 'r') as f:
    config = yaml.load(f, Loader=yaml.SafeLoader)


### TODOS ###
"""
- implement toggle effect logic
"""
skills = load_skills(config)
enemies = load_enemies(config)
weapons = load_weapons(config)
calculations = load_calculations(config)
damage_bonuses = load_bonuses(config)
gear_bonuses = dict(calculate_gear_bonuses(damage_bonuses['gear']))
bonus_effects = damage_bonuses['effects']
for calc in calculations:
    if calc == 'effects':
        continue
        active_effects = effects
    skill = skills[calc]
    weapon = weapons[calculations[calc]['weapon']]
    damage_bonus = 0
    damage_sum = 0
    original_skill_type = skill['type']
    for damage in weapon:
        enemy = enemies[calculations[calc]['enemy']]
        if skill['type'] == 'inherit' or original_skill_type == 'inherit':
            skill['type'] = weapon[damage]['type']

        for bonus in bonus_effects:
            if skill['type'] == bonus_effects[bonus]['type']:
                # apply bonus effect
                try:
                    if gear_bonuses[skill['type']]:
                        damage_bonus = gear_bonuses[skill['type']]
                except:
                    pass
                damage_bonus = damage_bonus + bonus_effects[bonus]['multiplier']

        damage_sum += calculate_damage(weapon[damage]['value'],
                                       skill['multiplier'],
                                       skill['skill_flat'],
                                       damage_bonus,
                                       enemy['resistances'][skill['type']])

    print(f'Running calculations for {calc} with '
          f'{calculations[calc]["weapon"]} against {enemy["name"]}')
    print(f'Total Damage: {damage_sum}')
    try:
        print(f'Attacks until enemy is dead: {round(enemy["health"] / damage_sum)}')
    except:
        print('Weapon does no damage')
